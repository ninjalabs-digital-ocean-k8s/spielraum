<div id="content" class="bg-green-500 text-white overflow-hidden">
  <div class="flex flex-col items-center justify-start relative" style="min-height: 90vh;">
    <img src="<?php echo $config->urls->files ?>/pattern.svg" class="hidden md:block w-64 absolute top-0 right-0 m-8" alt="">

    <div class="text-5xl font-pacifico tracking-wider p-16">
      <?php echo $page->headline ?>
    </div>

    <img src="<?php echo $page->image->url ?>" class="border-white border-8 rounded-lg max-w-md mt-8" style="transform: rotate(-7deg)">
  </div>

  <div class="text-center flex flex-col p-4 container mx-auto max-w-2xl custom-body">
    <?php echo $page->body ?>
  </div>

  <div class="mt-16 p-4 container mx-auto max-w-2xl flex flex-col md:flex-row items-center justify-start">
    <img src="<?php echo $config->urls->files ?>/pdf.svg" class="w-24 p-4 self-end -mb-16 md:self-center md:mb-0" style="transform: rotate(7deg)">
    <div class="flex flex-col md:ml-4">
      <div class="font-pacifico text-2xl">
        Konzept
      </div>
      <div class="mt-4">
        Hier kannst du dir mein aktuelles Konzept herunterladen und anschauen!
      </div>
      <a href="<?php echo $page->concept->url ?>" target="_blank" download="konzept-spielraum.pdf" class="cursor-pointer select-none border-white border-2 px-4 py-2 rounded-lg font-bold tracking-wide text-center mt-4 hover:text-green-500 hover:bg-white uppercase">
        Konzept herunterladen
      </a>
    </div>
  </div>

  <div class="bg-green-800 mt-32 p-4 py-16">
    <div class="text-center flex flex-col container mx-auto max-w-2xl">
      <div class="font-pacifico text-2xl">
        Kontakt
      </div>
      <div class="mt-8">
        Susanne Dittrich<br>
        Kronenstraße 25<br>
        01129 Dresden<br><br>

        Telefon: 0351-8435203<br>
        Handy: 0151-56023743<br>
      </div>
    </div>
    <div class="text-center flex flex-col items-center container mx-auto max-w-2xl mt-16">
      <a href="/processwire" class="font-bold text-green-400 border-b-2 border-green-400">Anmelden (Intern)</a>
    </div>
  </div>
</div>
