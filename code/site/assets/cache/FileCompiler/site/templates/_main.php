<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates ?>styles/main.css" />
  <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@400;700&family=Pacifico&display=swap" rel="stylesheet">
  <title><?php echo $page->title ?></title>
</head>

<body>
  <div id="content">
    <p>Main content</p>
  </div>
</body>

</html>
